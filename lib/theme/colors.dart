import 'package:flutter/material.dart';

class ColorsTheme{
  final Color green = const Color.fromRGBO(97, 210, 182, 1);
  final Color blue = const Color.fromRGBO(18, 63, 96, 1);
  final Color grey = const Color.fromRGBO(158, 161, 175, 1);
  final Color greyObscure = const Color.fromRGBO(88, 94, 104, 1);
}