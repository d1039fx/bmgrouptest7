import 'package:flutter/material.dart';

import '../theme/colors.dart';

class GridActionPower with ColorsTheme{
  List<Map<String, dynamic>> listActionPower(){
    final List<Map<String, dynamic>> actionPowers = [
      {
        'name': 'Logo Design',
        'text_color': Colors.white,
        'icon_color': blue,
        'background_icon_color': Colors.white,
        'background_color': blue,
        'url_image' : 'https://limagemarketing.es/wp-content/uploads/la-importancia-del-logotipo.jpg'
      },
      {
        'name': 'Corporate Identity',
        'text_color': blue,
        'icon_color': green,
        'background_icon_color': blue,
        'background_color': green,
        'url_image' : 'https://as1.ftcdn.net/v2/jpg/01/08/48/98/1000_F_108489877_FseVdxpNFUz96NPExBmY6FrGdbnAIo7N.jpg'
      },
      {
        'name': 'Influencer Guide',
        'text_color': blue,
        'icon_color': green,
        'background_icon_color': blue,
        'background_color': green,
        'url_image': 'https://newsfeed.org/wp-content/uploads/Instagram-Influencer-marketing-tutorial.jpg'
      },
      {
        'name': 'Project Management',
        'text_color': Colors.white,
        'icon_color': blue,
        'background_icon_color': Colors.white,
        'background_color': blue,
        'url_image':'https://www.argentinaproduct.com/ckfinder/userfiles/files/blog/projectmanagement.jpg'
      },
      {
        'name': 'Content Writer',
        'text_color': Colors.white,
        'icon_color': Colors.grey[700],
        'background_icon_color': Colors.white,
        'background_color': Colors.grey[700],
        'url_image' : 'https://miro.medium.com/max/2000/1*EioSQpM0_jj-VeOPpcNubw.jpeg'
      },
      {
        'name': 'Experimental Illustration',
        'text_color': blue,
        'icon_color': green,
        'background_icon_color': blue,
        'background_color': green,
        'url_image': 'https://www.doodlersanonymous.com/images/entries/medium/2599_instagram-experiments_0046.jpg'
      },
    ];

    return actionPowers;
  }
}