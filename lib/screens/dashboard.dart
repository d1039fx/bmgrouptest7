import 'package:flutter/material.dart';
import 'package:tes_bmgroup_7/widgets/appbar_dashboard.dart';
import 'package:tes_bmgroup_7/widgets/info_user/Info_user.dart';

import '../widgets/personal_interest/content_interest.dart';

class DashBoard extends StatelessWidget {
  const DashBoard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 80,
        backgroundColor: Colors.white,
        title:  AppBarDashboard(),
      ),
      body: Row(
        children:  [
          const SizedBox(width: 420,child: InfoUser(),),
          Expanded(flex: 3,child: ContentInterest())
        ],
      ),
    );
  }
}
