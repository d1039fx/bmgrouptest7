import 'package:flutter/material.dart';
import 'package:tes_bmgroup_7/widgets/info_user/middle_layouts/current_data.dart';
import 'package:tes_bmgroup_7/widgets/info_user/middle_layouts/powerlevel_reward_data.dart';
import 'package:tes_bmgroup_7/widgets/info_user/middle_layouts/user_data.dart';

class MiddleLayout extends StatelessWidget {
  const MiddleLayout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: Column(
        children: [
          Container(
            margin: const EdgeInsets.only(bottom: 15),
            child: UserData(),
          ),
          const Divider(),
           CurrentData(),
          const Divider(
            height: 30,
          ),
          Expanded(
            child: Container(
                margin: const EdgeInsets.only(top: 10),
                child:  PowerRewardData()),
          )
        ],
      ),
    );
  }
}
