import 'package:flutter/material.dart';
import 'package:tes_bmgroup_7/theme/colors.dart';

class BottomLayout extends StatelessWidget with ColorsTheme {
  BottomLayout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 50, vertical: 15),
      decoration: BoxDecoration(
          border: Border.all(color: Colors.transparent),
          borderRadius: BorderRadius.circular(50),
          color: Colors.grey[300]),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.account_circle,
                color: greyObscure,
              )),
          IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.event_available,
                color: greyObscure,
              )),
          IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.settings,
                color: greyObscure,
              )),
        ],
      ),
    );
  }
}
