import 'package:flutter/material.dart';
import 'package:tes_bmgroup_7/theme/colors.dart';

class SuperiorLayoutInfoUser extends StatelessWidget with ColorsTheme {
  SuperiorLayoutInfoUser({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[300],
      height: 60,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          MaterialButton(
            onPressed: () {},
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.content_copy_rounded,
                  size: 40,
                  color: green,
                ),
                Text(
                  'ActionPower',
                  style: TextStyle(fontSize: 11, color: green),
                )
              ],
            ),
          ),
          MaterialButton(
            onPressed: () {},
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.grass,
                  size: 40,
                  color: greyObscure,
                ),
                Text(
                  'SeedCoins',
                  style: TextStyle(fontSize: 11, color: grey),
                )
              ],
            ),
          ),
          MaterialButton(
            onPressed: () {},
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(Icons.interpreter_mode, size: 40, color: greyObscure),
                Text(
                  'WeSocial',
                  style: TextStyle(fontSize: 11, color: grey),
                )
              ],
            ),
          ),
          MaterialButton(
            onPressed: () {},
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(Icons.sms_outlined, size: 40, color: greyObscure),
                Text(
                  'WeChat',
                  style: TextStyle(fontSize: 11, color: grey),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
