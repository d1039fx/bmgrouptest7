import 'package:flutter/material.dart';
import 'package:tes_bmgroup_7/theme/colors.dart';

class UserData extends StatelessWidget with ColorsTheme{
  UserData({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Stack(
          children: [
            const Icon(
              Icons.account_circle,
              color: Colors.grey,
              size: 130,
            ),
            Positioned(
                bottom: 10,
                right: 10,
                child: CircleAvatar(
                  backgroundColor: green,
                  radius: 18,
                  child: IconButton(
                      onPressed: () {},
                      icon: const Icon(
                        Icons.payments,
                        size: 20,
                        color: Colors.white,
                      )),
                ))
          ],
        ),
         Text(
          'Milton Oviedo',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: greyObscure),
        ),
        const SizedBox(
          height: 8,
        ),
         Text('Entrepreneur - PowerLevel 8', style: TextStyle(color: grey),)
      ],
    );
  }
}
