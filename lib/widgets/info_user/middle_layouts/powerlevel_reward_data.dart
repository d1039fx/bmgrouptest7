import 'package:flutter/material.dart';
import 'package:tes_bmgroup_7/theme/colors.dart';

class PowerRewardData extends StatelessWidget with ColorsTheme {
  PowerRewardData({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              'Your next PowerLevel',
              style: TextStyle(color: grey),
            ),
            const SizedBox(
              height: 10,
            ),
            Stack(
              alignment: Alignment.centerRight,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(50),
                  child: LinearProgressIndicator(
                    value: 50, minHeight: 18,
                    color: green,
                    //valueColor: Colors.green,
                  ),
                ),
                Positioned(
                    child: CircleAvatar(
                      radius: 15,
                      backgroundColor: green,
                  child: CircleAvatar(
                    backgroundColor: Colors.white,
                    radius: 13,
                    child: Text(
                      '9',
                      style: TextStyle(color: green, fontWeight: FontWeight.bold),
                    ),
                  ),
                ))
              ],
            )
          ],
        ),
        const SizedBox(height: 20,),
        Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              'Next Reward',
              style: TextStyle(color: grey),
            ),
            const SizedBox(
              height: 10,
            ),
            Stack(
              alignment: Alignment.centerRight,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(50),
                  child: LinearProgressIndicator(
                    value: 50, minHeight: 18,
                    color: blue,
                  ),
                ),
                Positioned(
                    child: CircleAvatar(
                      radius: 15,
                      backgroundColor: blue,
                      child: CircleAvatar(
                        backgroundColor: Colors.white,
                        radius: 13,
                        child: Text(
                          '2',
                          style: TextStyle(color: blue, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ))
              ],
            )
          ],
        ),
      ],
    );
  }
}
