import 'package:flutter/material.dart';

import '../../../theme/colors.dart';

class CurrentData extends StatelessWidget with ColorsTheme {
  CurrentData({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Image.asset('assets/images/plant.png', height: 100,),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    'Current Balance',
                    style: TextStyle(color: grey),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Text(
                    '200 SeedCoins',
                    style: TextStyle(fontSize: 20, color: green),
                  )
                ],
              ),
            ),
          ],
        ),
        MaterialButton(
          textColor: Colors.grey[700],
          onPressed: () {},
          shape: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(50))),
          padding: const EdgeInsets.symmetric(horizontal: 38),
          child: const Text(
            'Add 50 SeedCoins',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        )
      ],
    );
  }
}
