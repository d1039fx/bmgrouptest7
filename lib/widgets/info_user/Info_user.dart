import 'package:flutter/material.dart';
import 'package:tes_bmgroup_7/widgets/info_user/bottom_layout.dart';
import 'package:tes_bmgroup_7/widgets/info_user/middle_layout.dart';
import 'package:tes_bmgroup_7/widgets/info_user/superior_layout.dart';

class InfoUser extends StatelessWidget {
  const InfoUser({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 40, top: 20, right: 40, bottom: 40),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey[300]!, width: 3),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children:  [
          SuperiorLayoutInfoUser(),
          Expanded(child: MiddleLayout()),
          BottomLayout()
        ],
      ),
    );
  }
}
