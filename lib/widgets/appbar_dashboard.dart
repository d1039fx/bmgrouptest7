import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tes_bmgroup_7/theme/colors.dart';
import 'package:tes_bmgroup_7/widgets/appbar/search_country_notification_menu.dart';

class AppBarDashboard extends StatelessWidget with ColorsTheme {
  AppBarDashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Row(
            children: [
              Image.asset('assets/images/principal_logo.png'),
              Container(
                height: 40,
                width: 2,
                margin: const EdgeInsets.symmetric(horizontal: 20),
                decoration: BoxDecoration(
                    border:
                        Border(right: BorderSide(color: Colors.grey[300]!))),
              ),
              Text.rich(TextSpan(
                  text: 'wake up ',
                  children: [
                    TextSpan(
                        text: 'your dreams',
                        style: TextStyle(
                            color: green, fontWeight: FontWeight.bold))
                  ],
                  style: TextStyle(color: blue, fontWeight: FontWeight.bold))),
            ],
          ),
        ),
        SearchCountryNotificationMenu()
      ],
    );
  }
}
