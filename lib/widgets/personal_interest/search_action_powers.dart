import 'package:flutter/material.dart';
import 'package:tes_bmgroup_7/theme/colors.dart';

class SearchActionPowers extends StatelessWidget with ColorsTheme {
  SearchActionPowers({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController searchActionPowers = TextEditingController();
    GlobalKey<FormState> form = GlobalKey<FormState>();

    return Container(
      height: 75,
      color: green,
      child: Center(
        child: Form(
            key: form,
            child: Row(
              children: [
                Container(
                  width: 120,
                  height: 50,
                  margin: const EdgeInsets.only(left: 200),
                  decoration: BoxDecoration(
                      color: Colors.grey[300],
                      border: Border.all(color: Colors.transparent, width: 1),
                      borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(50),
                          bottomLeft: Radius.circular(50))),
                  child: MaterialButton(
                    onPressed: () {
                      print('search actions powers');
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: const [
                        Text('See All'),
                        Icon(Icons.keyboard_arrow_down)
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                      margin: const EdgeInsets.only(right: 200),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border:
                              Border.all(color: Colors.transparent, width: 1),
                          borderRadius: const BorderRadius.only(
                              topRight: Radius.circular(50),
                              bottomRight: Radius.circular(50))),
                      child: TextFormField(
                        controller: searchActionPowers,
                        decoration: InputDecoration(
                            contentPadding: const EdgeInsets.only(top: 0, bottom: 0, left: 20),
                            focusedBorder: const OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                              borderSide: BorderSide(
                                  width: 1, color: Colors.transparent),
                            ),
                            enabledBorder: const OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                              borderSide: BorderSide(
                                  width: 1, color: Colors.transparent),
                            ),
                            hintText: 'Find my ActionPowers',
                            suffixIcon: IconButton(
                                onPressed: () {},
                                icon: Icon(
                                  Icons.search,
                                  color: green,
                                )),
                            border: const OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.red, style: BorderStyle.none),
                                borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(50),
                                    bottomRight: Radius.circular(50)))),
                      )),
                ),
              ],
            )),
      ),
    );
  }
}
