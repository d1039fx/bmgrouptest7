import 'package:flutter/material.dart';
import 'package:tes_bmgroup_7/repository/grid_action_power.dart';
import 'package:tes_bmgroup_7/theme/colors.dart';

class GridInterest extends StatelessWidget with ColorsTheme {
  GridInterest({Key? key}) : super(key: key);

  final List<Map<String, dynamic>> actionPowers =
      GridActionPower().listActionPower();

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10),
      child: GridView.builder(
          itemCount: actionPowers.length,
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              childAspectRatio: 1,
              crossAxisSpacing: 20,
              mainAxisSpacing: 20,
              mainAxisExtent: 160),
          itemBuilder: (context, index) {
            Map<String, dynamic> actions = actionPowers[index];
            return SizedBox(

              child: Row(
                children: [
                  Expanded(
                      child: Image.network(
                    actions['url_image'],
                    fit: BoxFit.cover,
                  )),
                  Expanded(
                      child: Container(
                    color: actions['background_color'],
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Expanded(
                            child: Align(
                          alignment: Alignment.centerLeft,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              actions['name'],
                              style: TextStyle(
                                  color: actions['text_color'],
                                  fontSize: 30,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        )),
                        Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CircleAvatar(
                                  backgroundColor:
                                      actions['background_icon_color'],
                                  child: IconButton(
                                      padding: const EdgeInsets.all(0),
                                      onPressed: () {},
                                      icon: Icon(
                                        Icons.arrow_right,
                                        size: 40,
                                        color: actions['icon_color'],
                                      ))),
                              Container(
                                margin:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                child: CircleAvatar(
                                    backgroundColor:
                                        actions['background_icon_color'],
                                    child: IconButton(
                                        onPressed: () {},
                                        icon: Icon(
                                          Icons.check_box_outline_blank,
                                          color: actions['icon_color'],
                                        ))),
                              ),
                              CircleAvatar(
                                  backgroundColor:
                                      actions['background_icon_color'],
                                  child: IconButton(
                                      onPressed: () {},
                                      icon: Icon(
                                        Icons.card_giftcard,
                                        color: actions['icon_color'],
                                      ))),
                            ],
                          ),
                        )
                      ],
                    ),
                  ))
                ],
              ),
            );
          }),
    );
  }
}
