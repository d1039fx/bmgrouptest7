import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:tes_bmgroup_7/theme/colors.dart';

class InterestList extends StatelessWidget with ColorsTheme {
  InterestList({Key? key}) : super(key: key);

  final List<Map<String, dynamic>> listInterest = [
    {'name': 'Growth', 'icon': const Icon(Icons.upload)},
    {'name': 'Instant', 'icon': const Icon(Icons.arrow_forward_outlined)},
    {'name': 'Lifestyle', 'icon': const Icon(Icons.hardware_outlined)},
    {'name': 'Inspirational', 'icon': const Icon(Icons.important_devices)},
    {'name': 'Experience', 'icon': const Icon(Icons.ac_unit)},
    {'name': 'Together', 'icon': const Icon(Icons.access_alarms_rounded)},
  ];

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 80,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: listInterest
            .map<Widget>((e) => Container(
                  color: Colors.grey[200],
                  width: 150,
                  margin: listInterest.indexWhere(
                              (element) => element['name'] == e['name']) == 0
                      ? const EdgeInsets.only(right: 10, top: 10, bottom: 10)
                      : const EdgeInsets.all(10),
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      MaterialButton(
                        onPressed: () {
                          if (kDebugMode) {
                            print(e['name']);
                          }
                        },
                        child: Row(
                          children: [
                            CircleAvatar(
                              child: e['icon'],
                              backgroundColor: green,
                              foregroundColor: greyObscure,
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Text(
                              e['name'],
                              style:
                                  const TextStyle(fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                      Positioned(
                          bottom: 5,
                          right: 5,
                          child: Icon(
                            Icons.arrow_forward_ios,
                            size: 15,
                            color: green,
                          ))
                    ],
                  ),
                ))
            .toList(),
      ),
    );
  }
}
