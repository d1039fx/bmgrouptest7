import 'package:flutter/material.dart';
import 'package:tes_bmgroup_7/theme/colors.dart';
import 'package:tes_bmgroup_7/widgets/personal_interest/search_action_powers.dart';

import 'grid_interest.dart';
import 'interets_list.dart';

class ContentInterest extends StatelessWidget with ColorsTheme {
  ContentInterest({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            margin: const EdgeInsets.symmetric(vertical: 20),
            child: Center(
              child: Text(
                'Explore your Personal Interest',
                style: TextStyle(color: green, fontSize: 30),
              ),
            ),
          ),
          InterestList(),
          SearchActionPowers(),
          Expanded(child: GridInterest())
        ],
      ),
    );
  }
}
