import 'package:flutter/material.dart';

class SearchCountryNotificationMenu extends StatelessWidget {
  SearchCountryNotificationMenu({Key? key}) : super(key: key);

  final TextEditingController search = TextEditingController();
  final GlobalKey<FormState> form = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        MaterialButton(
            onPressed: () {},
            child: Row(
              children: const [
                CircleAvatar(),
                SizedBox(
                  width: 8,
                ),
                Text('CO')
              ],
            )),
        const SizedBox(
          width: 20,
        ),
        Form(
            child: Container(
                decoration: BoxDecoration(
                    color: Colors.grey[100],
                    border: Border.all(color: Colors.transparent),
                    borderRadius: BorderRadius.circular(20)),
                width: 300,
                child: TextFormField(
                  validator: (value) {
                    if (value == null) {
                      return 'Ingrese la busqueda';
                    } else {
                      return null;
                    }
                  },
                  onChanged: (changeText) {
                    print(changeText);
                  },
                  decoration: InputDecoration(
                      contentPadding: const EdgeInsets.only(left: 30),
                      hintText: 'Search',
                      suffixIcon: IconButton(
                          onPressed: () {
                            if (form.currentState!.validate()) {
                              print(search.text);
                            }
                          },
                          icon: const Icon(
                            Icons.search,
                            color: Colors.blue,
                          )),
                      border: OutlineInputBorder(
                          borderSide: const BorderSide(color: Colors.green),
                          borderRadius: BorderRadius.circular(20))),
                ))),
        const SizedBox(
          width: 20,
        ),
        IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.assignment_late,
              color: Colors.grey[700],
            )),
        IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.email,
              color: Colors.grey[700],
            )),
        IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.notifications,
              color: Colors.grey[700],
            )),
        PopupMenuButton<Menu>(color: Colors.grey[100],
            child: Row(
              children: const [
                Icon(
                  Icons.account_circle,
                  size: 70,
                  color: Colors.grey,
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  'Milton Oviedo',
                  style: TextStyle(color: Colors.grey, fontSize: 17),
                ),
                SizedBox(
                  width: 8,
                ),
                Icon(Icons.expand_more, color: Colors.grey,),
              ],
            ),
            itemBuilder: (context) => <PopupMenuEntry<Menu>>[
                  const PopupMenuItem<Menu>(
                    value: Menu.itemOne,
                    child: Text('Item 1'),
                  ),
                  const PopupMenuItem<Menu>(
                    value: Menu.itemTwo,
                    child: Text('Item 2'),
                  ),
                  const PopupMenuItem<Menu>(
                    value: Menu.itemThree,
                    child: Text('Item 3'),
                  ),
                  const PopupMenuItem<Menu>(
                    value: Menu.itemFour,
                    child: Text('Item 4'),
                  ),
                ])
      ],
    );
  }
}

enum Menu { itemOne, itemTwo, itemThree, itemFour }
