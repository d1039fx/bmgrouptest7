import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tes_bmgroup_7/screens/dashboard.dart';

void main() {
  runApp(const TestBmGroup7());
}

class TestBmGroup7 extends StatelessWidget {
  const TestBmGroup7({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        fontFamily: GoogleFonts.montserrat().fontFamily,
        // textTheme:  TextTheme(
        //   bodyText1: GoogleFonts.montserrat()
        //
        // )
      ),
      home: const DashBoard(),
    );
  }
}
